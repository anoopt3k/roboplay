extends Button

signal timeout

var ros_talker = Talker.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	ros_talker.spin_some()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
	
func _toggled(button_pressed):
	if button_pressed:
		ros_talker.talk(1)
		print("button pressed")
		set_text("I'm Lit")
	else:
		ros_talker.talk(0)
		print("button unpressed")
		set_text("Lit Me")
	
