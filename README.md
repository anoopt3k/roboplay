# roboplay

This repo contains
- roboplaygame - A godot game to control robotis using ros
- thirdparty - Folder with all external software 
- thirdparty/godot - Godot game engine
- thirdparty/godot_ros - Godot ros plugin
- thirdparty/ros2_iron - ROS Iron Irwini source
- thirdparty/sip and pyqt5 - PyQT5 and SIP for ROS2 gui dependencies.

# Setup

  - Install pyenv with python 3.10
  - See ros2 and godot for apt-get dependencies

# Build
 

  make build-ros2

  make setup #Shows file to source for setting up ros2
  source <path/to/local.bash> #as shown above

  make build-godot #builds godot with godot_ros plugin

# Run

  make run #launces godot game engine
  open roboplaygame in godot and run it

  make listen #another window to see ros2 messages when game controls are used
