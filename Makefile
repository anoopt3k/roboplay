################################################################## 
#  ROS2 Build Script
################################################################## 

.ONESHELL:
SHELL := /bin/bash

ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
GODOT_DIR = $(ROOT_DIR)/thirdparty/godot
GODOT_ROS_DIR = $(ROOT_DIR)/thirdparty/godot_ros
ROS2_DIR = $(ROOT_DIR)/thirdparty/ros2_iron
PYENV_ROOT := $(shell pyenv root) 
export ROS_DISTRO=iron
export PYTHONWARNINGS=ignore::DeprecationWarning

pyreinstall:
	@echo "Pristine: Reinstall pyenv."
	pyenv uninstall 3.10.9
	sleep 1
	pyenv install 3.10.9
	pyenv global 3.10.9
	pip3 install --upgrade pip

download: 
	vcs import --input https://raw.githubusercontent.com/ros2/ros2/iron/ros2.repos $(ROS2_DIR)/src
	wget https://sourceforge.net/projects/pyqt/files/sip/sip-4.19.13/sip-4.19.13.tar.gz -P $(ROOT_DIR)/thirdparty
	wget https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.11.3/PyQt5_gpl-5.11.3.tar.gz -P $(ROOT_DIR)/thirdparty
	T
	cd $(ROOT_DIR)/thirdparty; tar xzf sip-4.19.13.tar.gz
	cd $(ROOT_DIR)/thirdparty; tar xzf PyQt5_gpl-5.11.3.tar.gz
	cd $(ROOT_DIR)/thirdparty; git clone https://github.com/flynneva/godot_ros.git
	cd $(ROOT_DIR)/thirdparty; git clone https://github.com/godotengine/godot.git

clean:
	@echo "Clean. Remove directories. Uninstall all py packages"
	-pip3 cache purge
	-pip3 freeze | xargs pip3 uninstall -y
	-sudo rm -rf /etc/ros/rosdep
	-rm -rf $(ROS2_DIR)/build $(ROS2_DDIR)/logs $(ROS2_DIR)/install
	-cd $(ROOT_DIR)/thirdparty/sip-4.19.13; make clean
	-cd $(ROOT_DIR)/thirdparty/PyQt5_gpl-5.11.3; make clean

# install python dependencies
pydep:
	@echo "Installing python dependencies"
	pip3 install -U setuptools==59.6.0
	pip3 install -U  argcomplete bloom catkin_pkg colcon-common-extensions coverage  cryptography empy flake8 flake8-blind-except==0.1.1 flake8-builtins flake8-class-newline flake8-comprehensions flake8-deprecated flake8-docstrings flake8-import-order flake8-quotes ifcfg importlib-metadata jsonschema lark==1.1.1 lxml matplotlib mock mypy==0.931 netifaces nose numpy pep8 psutil pydocstyle pydot pyparsing==2.4.7 pytest-repeat pytest-rerunfailures pytest-mock rosdep rosdistro vcstool 

# build sip
sip:
	@if  [[ `pip3 list | grep -i sip` ]]; then 
		@echo "Sip already installed $(ROOT_DIR)/thirdparty/sip-4.19.13"
	else 
		@echo "Building sip at $(ROOT_DIR)/thirdparty/sip-4.19.13"
		cd $(ROOT_DIR)/thirdparty/sip-4.19.13; python ./configure.py; make; make install 
	fi;

# build pyqt5 with sip
pyqt5:
	@if  [[ `pip3 list | grep -i pyqt5` ]]; then
		@echo "PyQ5 already installed $(ROOT_DIR)/thirdparty/PyQt5_gpl-5.11.3"
	else
		@echo "Building pyqt5 at $(ROOT_DIR)/thirdparty/PyQt5_gpl-5.11.3"
		cd $(ROOT_DIR)/thirdparty/PyQt5_gpl-5.11.3; python ./configure.py --sip $(PYENV_ROOT)/versions/3.10.9/bin/sip ; make -j4 ; make install
	fi

# dep
rosdep: 
	-sudo rosdep init
	rosdep update
	cd $(ROS2_DIR); rosdep install --from-paths src --ignore-src -y --skip-keys "fastcdr rti-connext-dds-6.0.1 urdfdom_headers ignition-math6 ignition-cmake2 python3-catkin-pkg-modules python3-rosdistro-modules" --rosdistro iron

build-ros2: pydep rosdep sip pyqt5 
	cd $(ROS2_DIR); colcon build --symlink-install --packages-skip image_tools intra_process_demo python_orocos_kdl_vendor qt_gui_cpp qt_gui_core rqt_gui_cpp rqt

build-godot: 
	cd $(GODOT_DIR);scons -j4 custom_modules=$(GODOT_ROS_DIR)

setup:
	@chmod +x $(ROS2_DIR)/install/local_setup.bash
	@echo "Add . $(ROS2_DIR)/install/local_setup.bash to .bashrc"

# TODO: Not working. need to fix setup to source the env before building godot.
build: build-ros2 setup build-godot

run: 
	$(GODOT_DIR)/bin/godot.linuxbsd.editor.x86_64

run-bot: 
	$(GODOT_DIR)/bin/godot.linuxbsd.editor.x86_64 games/botxbot/project.godot

listen: 
	ros2 topic echo /talker
